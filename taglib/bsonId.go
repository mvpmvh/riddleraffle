package taglib

import (
	"gopkg.in/mgo.v2/bson"
)

//template function to return string representation of bson id
func BsonId2String(bsonId bson.ObjectId) string {
	return bsonId.Hex()
}
