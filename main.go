/*
main server implementation to handle http requests using Beego framework
*/
package main

import (
	_ "bitbucket.org/rmmn/riddleraffle/routers"
	"bitbucket.org/rmmn/riddleraffle/taglib"
	"bitbucket.org/rmmn/riddleraffle/utilities/mongo"
	"github.com/mvpmvh/beego"
	"os"
)

func main() {
	initLogger()
	initMongo()
	initTemplateFuncs()
	beego.Run()
}

func initLogger() {
	beego.SetLevel(beego.LevelDebug)
	os.Mkdir("./log", os.ModePerm)
	beego.BeeLogger.SetLogger("file", `{"filename": "log/log"}`)
}

func initMongo() {
	beego.Info("Starting Mongo...")

	if err := mongo.Startup(); err != nil {
		beego.Critical("Mongo failed to start" + err.Error())
		panic(err)
	}

	beego.Info("Mongo Started")
}

func initTemplateFuncs() {
	beego.AddFuncMap("params", taglib.Params)
	beego.AddFuncMap("append", taglib.Append)
	beego.AddFuncMap("bsonId", taglib.BsonId2String)
}
