<html>
    <head>
        <link rel="stylesheet" type="text/css" href="static/css/bootstrap/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="static/css/main/main.css">
		
    </head>

    <body>	
		<div class="container">
		{{with .Raffle}}
			<ul class="list-group">
				{{range $index, $hint := .Riddle.Hints}}
					{{if lt $index $.Raffle.ActiveHints}}
  						<li class="list-group-item list-group-item-info">{{$hint}}</li>
					{{end}}
				{{end}}
			</ul>
			
	    	<form class="form-horizontal" role="form" action="/guess" method="POST">
				<h2 class="form-horizontal-heading text-center">Know The Answer?</h2>
				<input type="hidden" name="raffleId" id="raffleId" value="{{bsonId .Id}}" />
	  			
				<div class="form-group">	    			
	      	 		<input type="text" class="form-control" name="answer" id="answer" placeholder="Answer" required autofocus />	    			
	  			</div>
				<div class="form-group">
					<button type="submit" class="btn btn-lng btn-primary btn-block">Submit</button>	  			
				</div>
	        </form>
		{{end}}
		
		{{if .ErrorMessage}} 	
			<div class="alert alert-danger alert-dismissible fade in" role="alert">
  				<button type="button" class="close" data-dismiss="alert">
					<span aria-hidden="true">&times;</span>
					<span class="sr-only">Close</span>
				</button>
  				{{.ErrorMessage}}
			</div>
		{{end}}
		
		</div> <!-- /container -->
		
		<script src="/static/js/jquery/jquery-2.1.1.min.js"></script>
		<script src="/static/js/bootstrap/bootstrap.min.js"></script>
    </body>
</html>