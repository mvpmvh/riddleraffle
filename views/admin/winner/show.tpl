{{with .Raffle}}
<div class="form-group">
	<label for="answer" class="col-sm-2 control-label">Email</label>
	<div class="col-sm-10">
	{{with .Winner}}
		<input type="text" class="form-control" id="email" name="email" value="{{.Email}}">
	{{else}}
		<input type="text" class="form-control" id="email" name="email" placeholder="Email">
	</div>
	{{end}}
</div>
{{end}}