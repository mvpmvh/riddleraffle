	  <div class="form-group">
	    <label for="answer" class="col-sm-2 control-label">Answer</label>
	    <div class="col-sm-10">
	      <input type="text" class="form-control" id="answer" name="answer" placeholder="Answer" required>
	    </div>
	  </div>
	
	  <div class="form-group">
	    <label for="hint1" class="col-sm-2 control-label">Hint 1</label>
	    <div class="col-sm-10">
	      <input type="text" class="form-control" id="hint1" name="hint1" placeholder="Hint #1" required>
	    </div>
	  </div>
      <div class="form-group">
	    <label for="reason1" class="col-sm-2 control-label">Reason 1</label>
	    <div class="col-sm-10">
	      <input type="text" class="form-control" id="reason1" name="reason1" placeholder="Reason #1" required>
	    </div>
	  </div>
		
	  <div class="form-group">
	    <label for="hint2" class="col-sm-2 control-label">Hint 2</label>
	    <div class="col-sm-10">
	      <input type="text" class="form-control" id="hint2" name="hint2" placeholder="Hint #2" required>
	    </div>
	  </div>
	  <div class="form-group">
	    <label for="reason2" class="col-sm-2 control-label">Reason 2</label>
	    <div class="col-sm-10">
	      <input type="text" class="form-control" id="reason2" name="reason2" placeholder="Reason #2" required>
	    </div>
	  </div>
	
	  <div class="form-group">
	    <label for="hint3" class="col-sm-2 control-label">Hint 3</label>
	    <div class="col-sm-10">
	      <input type="text" class="form-control" id="hint3" name="hint3" placeholder="Hint #3" required>
	    </div>
	  </div>
	  <div class="form-group">
	    <label for="reason3" class="col-sm-2 control-label">Reason 3</label>
	    <div class="col-sm-10">
	      <input type="text" class="form-control" id="reason3" name="reason3" placeholder="Reason #3" required>
	    </div>
	  </div>
	
	  <div class="form-group">
	    <label for="hint4" class="col-sm-2 control-label">Hint 4</label>
	    <div class="col-sm-10">
	      <input type="text" class="form-control" id="hint4" name="hint4" placeholder="Hint #4" required>
	    </div>
	  </div>
	  <div class="form-group">
	    <label for="reason4" class="col-sm-2 control-label">Reason 4</label>
	    <div class="col-sm-10">
	      <input type="text" class="form-control" id="reason4" name="reason4" placeholder="Reason #4" required>
	    </div>
	  </div>
	
	  <div class="form-group">
	    <label for="hint5" class="col-sm-2 control-label">Hint 5</label>
	    <div class="col-sm-10">
	      <input type="text" class="form-control" id="hint5" name="hint5" placeholder="Hint #5" required>
	    </div>
	  </div>
	  <div class="form-group">
	    <label for="reason5" class="col-sm-2 control-label">Reason 5</label>
	    <div class="col-sm-10">
	      <input type="text" class="form-control" id="reason5" name="reason5" placeholder="Reason #5" required>
	    </div>
	  </div>
	
	  <div class="form-group">
	    <label for="hint6" class="col-sm-2 control-label">Hint 6</label>
	    <div class="col-sm-10">
	      <input type="text" class="form-control" id="hint6" name="hint6" placeholder="Hint #6" required>
	    </div>
	  </div>
	  <div class="form-group">
	    <label for="reason1" class="col-sm-2 control-label">Reason 6</label>
	    <div class="col-sm-10">
	      <input type="text" class="form-control" id="reason6" name="reason6" placeholder="Reason #6" required>
	    </div>
	  </div>