{{template "admin/base/base.tpl" .}}		
{{define "head"}}{{end}}
{{define "body"}}
	<table class="table table-hover">
   		<thead>
      		<tr>
      	 		<th>#Id</th>
        		<th>Answer</th>
      		</tr>
   		</thead>
   	
		<tbody>
		{{range .Riddles}}
      		<tr>
       			<td><a href="/admin/riddle/show/{{bsonId .Id}}" title="Details">{{bsonId .Id}}</a></td>
       			<td><a href="/admin/riddle/show/{{bsonId .Id}}" title="Details">{{.Answer}}</a></td>
      		</tr>
		{{end}}
   		</tbody>
  	</table>
{{end}}