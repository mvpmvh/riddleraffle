	{{with .Raffle}}
	<div class="form-group">
	    <label for="answer" class="col-sm-2 control-label">Answer</label>
	    <div class="col-sm-10">
	      <input type="text" class="form-control" id="answer" name="answer" value="{{.Riddle.Answer}}" required>
	    </div>
	  </div>
	
	  <div class="form-group">
	    <label for="hint1" class="col-sm-2 control-label">Hint 1</label>
	    <div class="col-sm-10">
	      <input type="text" class="form-control" id="hint1" name="hint1" value="{{index .Riddle.Hints 0}}" required>
	    </div>
	  </div>
      <div class="form-group">
	    <label for="reason1" class="col-sm-2 control-label">Reason 1</label>
	    <div class="col-sm-10">
	      <input type="text" class="form-control" id="reason1" name="reason1" value="{{index .Riddle.Reasons 0}}" required>
	    </div>
	  </div>
		
	  <div class="form-group">
	    <label for="hint2" class="col-sm-2 control-label">Hint 2</label>
	    <div class="col-sm-10">
	      <input type="text" class="form-control" id="hint2" name="hint2" value="{{index .Riddle.Hints 1}}" required>
	    </div>
	  </div>
	  <div class="form-group">
	    <label for="reason2" class="col-sm-2 control-label">Reason 2</label>
	    <div class="col-sm-10">
	      <input type="text" class="form-control" id="reason2" name="reason2" value="{{index .Riddle.Reasons 1}}" required>
	    </div>
	  </div>
	
	  <div class="form-group">
	    <label for="hint3" class="col-sm-2 control-label">Hint 3</label>
	    <div class="col-sm-10">
	      <input type="text" class="form-control" id="hint3" name="hint3" value="{{index .Riddle.Hints 2}}" required>
	    </div>
	  </div>
	  <div class="form-group">
	    <label for="reason3" class="col-sm-2 control-label">Reason 3</label>
	    <div class="col-sm-10">
	      <input type="text" class="form-control" id="reason3" name="reason3" value="{{index .Riddle.Reasons 2}}" required>
	    </div>
	  </div>
	
	  <div class="form-group">
	    <label for="hint4" class="col-sm-2 control-label">Hint 4</label>
	    <div class="col-sm-10">
	      <input type="text" class="form-control" id="hint4" name="hint4" value="{{index .Riddle.Hints 3}}" required>
	    </div>
	  </div>
	  <div class="form-group">
	    <label for="reason4" class="col-sm-2 control-label">Reason 4</label>
	    <div class="col-sm-10">
	      <input type="text" class="form-control" id="reason4" name="reason4" value="{{index .Riddle.Reasons 3}}" required>
	    </div>
	  </div>
	
	  <div class="form-group">
	    <label for="hint5" class="col-sm-2 control-label">Hint 5</label>
	    <div class="col-sm-10">
	      <input type="text" class="form-control" id="hint5" name="hint5" value="{{index .Riddle.Hints 4}}" required>
	    </div>
	  </div>
	  <div class="form-group">
	    <label for="reason5" class="col-sm-2 control-label">Reason 5</label>
	    <div class="col-sm-10">
	      <input type="text" class="form-control" id="reason5" name="reason5" value="{{index .Riddle.Reasons 4}}" required>
	    </div>
	  </div>
	
	  <div class="form-group">
	    <label for="hint6" class="col-sm-2 control-label">Hint 6</label>
	    <div class="col-sm-10">
	      <input type="text" class="form-control" id="hint6" name="hint6" value="{{index .Riddle.Hints 5}}" required>
	    </div>
	  </div>
	  <div class="form-group">
	    <label for="reason1" class="col-sm-2 control-label">Reason 6</label>
	    <div class="col-sm-10">
	      <input type="text" class="form-control" id="reason6" name="reason6" value="{{index .Riddle.Reasons 5}}" required>
	    </div>
	  </div>
	{{end}}