<!DOCTYPE html>
<html>
	<head>
		{{template "admin/base/header.tpl" .}}
		{{template "head" .}}
		
		<script src="/static/js/jquery/jquery-2.1.1.min.js"></script>
		<script src="/static/js/bootstrap/bootstrap.min.js"></script>
		<style>
			.outer-wrapper {
				margin-top:65px;
				width:90%;				
			}
		</style>
	</head>
	
	<body>

        <div class="navbar navbar-default navbar-fixed-top" role="navigation">
			<div class="container">
				<div class="navbar-header">
	    			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
	        			<span class="sr-only">Toggle navigation</span>
	        			<span class="icon-bar"></span>
	        			<span class="icon-bar"></span>
	        		</button>
	        
					<a class="navbar-brand" href="/admin/">Riddle Raffle</a>
	    		</div>
			</div>
		</div>	

		<div class="outer-wrapper"> 
			{{template "body" .}}
		</div>				
	</body>
</html>
