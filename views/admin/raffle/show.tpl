{{template "admin/base/base.tpl" .}}
{{define "head"}}{{end}}
{{define "body"}}
	<form class="form-horizontal" role="form" method="post" action="/admin/update/{{bsonId .Raffle.Id}}">
		<input type="hidden" name="raffleId" id="raffleId" value="{{bsonId .Raffle.Id}}" />
		<div class="panel-group" id="accordion">
			<div class="panel panel-default">
    			<div class="panel-heading">
      				<h4 class="panel-title">
        				<a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
          					Raffle
        				</a>
      				</h4>
    			</div>
    			<div id="collapseOne" class="panel-collapse collapse in">
      				<div class="panel-body">
						<div class="form-group">
    						<label for="status" class="col-sm-2 control-label">Status</label>
							<div class="col-sm-10">
								<select id="status" name="status" class="form-control">
									{{range $status := .Statuses}}
  										<option value="{{$status}}" {{if eq $.Raffle.Status $status}} selected="selected"{{end}}>{{$status}}</option>
			  						{{end}}
								</select>
							</div>
						</div>
						<div class="form-group">
    						<label for="activeHints" class="col-sm-2 control-label">Active Hints</label>
							<div class="col-sm-10">
		  						<select id="activeHints" name="activeHints" class="form-control">
									{{range $hint := .Hints}}
  										<option value="{{$hint}}" {{if eq $.Raffle.ActiveHints $hint}} selected="selected"{{end}}>{{$hint}}</option>
			  						{{end}}
								</select>
							</div>
						</div>        				
      				</div>
    			</div>
  			</div>
			<div class="panel panel-default">
	    		<div class="panel-heading">
      				<h4 class="panel-title">
        				<a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
          					Riddle
        				</a>
      				</h4>
    			</div>
    			<div id="collapseTwo" class="panel-collapse collapse">
      				<div class="panel-body">
      					{{template "admin/riddle/show.tpl" .}}
      				</div>
    			</div>
  			</div>
			<div class="panel panel-default">
    			<div class="panel-heading">
      				<h4 class="panel-title">
        				<a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
          					Prize
        				</a>
      				</h4>
    			</div>
    			<div id="collapseThree" class="panel-collapse collapse">
      				<div class="panel-body">
        				{{template "admin/prize/show.tpl" .}}
      				</div>
    			</div>
  			</div>
			<div class="panel panel-default">
    			<div class="panel-heading">
      				<h4 class="panel-title">
        				<a data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
          					Winner
        				</a>
      				</h4>
    			</div>
    			<div id="collapseFour" class="panel-collapse collapse">
      				<div class="panel-body">
        				{{template "admin/winner/show.tpl" .}}
      				</div>
    			</div>
  			</div>
		</div>				  
	
	  	<div class="form-group">
	    	<div class="col-sm-offset-2 col-sm-10">
	      		<button type="submit" class="btn btn-default">Update</button>
	    	</div>
	  	</div>
	</form>
{{end}}