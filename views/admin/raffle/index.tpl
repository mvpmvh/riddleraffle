{{template "admin/base/base.tpl" .}}		
{{define "head"}}{{end}}
{{define "body"}}
	<table class="table table-hover">
   		<thead>
      		<tr>
      	 		<th>#</th>
        			<th>Status</th>
        			<th>Active Hints</th>
      		</tr>
   		</thead>
   	
		<tbody>
		{{range .Raffles}}
      		<tr>
       			<td><a href="/admin/show/{{bsonId .Id}}" title="Details">{{bsonId .Id}}</a></td>
       			<td>{{.Status}}</td>
       			<td>{{.ActiveHints}}</td>
      		</tr>
		{{end}}
   		</tbody>
  	</table>
{{end}}