{{with .Raffle}}
<div class="form-group">
	{{with .Prize}}
	<label for="url" class="col-sm-2 control-label">URL</label>
	<div class="col-sm-10">	
		<input type="text" class="form-control" id="prizeURL" name="prizeURL" value="{{.URL}}">
	</div>
	<label for="title" class="col-sm-2 control-label">Title</label>
	<div class="col-sm-10">
		<input type="text" class="form-control" id="prizeTitle" name="prizeTitle" value="{{.Title}}">
	</div>
	<label for="photo" class="col-sm-2 control-label">Photo</label>
	<div class="col-sm-10">
		<input type="text" class="form-control" id="prizePhoto" name="prizePhoto" value="{{.Photo}}">
	</div>
	{{else}}
	<label for="url" class="col-sm-2 control-label">URL</label>
	<div class="col-sm-10">	
		<input type="text" class="form-control" id="prizeURL" name="prizeURL" placeholder="URL">
	</div>
	<label for="title" class="col-sm-2 control-label">Title</label>
	<div class="col-sm-10">
		<input type="text" class="form-control" id="prizeTitle" name="prizeTitle" placeholder="Title">
	</div>
	<label for="photo" class="col-sm-2 control-label">Photo</label>
	<div class="col-sm-10">
		<input type="text" class="form-control" id="prizePhoto" name="prizePhoto" placeholder="Photo">
	</div>
	{{end}}
</div>
{{end}}