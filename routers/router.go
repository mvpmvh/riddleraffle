package routers

import (
	"bitbucket.org/rmmn/riddleraffle/controllers"
	"github.com/mvpmvh/beego"
)

func init() {
	beego.Router("/", &controllers.MainController{}, "get:Welcome")
	beego.Router("/guess", &controllers.MainController{}, "post:Guess")

	beego.Router("/admin/", &controllers.RaffleController{}, "*:List")
	beego.Router("/admin/create", &controllers.RaffleController{}, "get:Create;post:Save")
	beego.Router("/admin/show/:id", &controllers.RaffleController{}, "*:Show")
	beego.Router("/admin/update/:id", &controllers.RaffleController{}, "post:Update")
}
