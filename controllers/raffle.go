package controllers

import (
	"bitbucket.org/rmmn/riddleraffle/models"
	"bitbucket.org/rmmn/riddleraffle/services"
	"github.com/mvpmvh/beego"
	"gopkg.in/mgo.v2/bson"
)

const (
	PENDING_STATUS  = "Pending"
	ACTIVE_STATUS   = "Active"
	FINISHED_STATUS = "Finished"
)

type RaffleController struct {
	beego.Controller
	raffleService *services.RaffleService
}

func (this *RaffleController) Prepare() {
	if this.raffleService == nil {
		this.raffleService = new(services.RaffleService)
	}
}

func (this *RaffleController) List() {
	raffles := this.raffleService.FindRafflesWithLimit(10)
	this.Data["Raffles"] = &raffles
	this.TplNames = "admin/raffle/index.tpl"
}

func (this *RaffleController) Create() {
	this.TplNames = "admin/raffle/create.tpl"
}

func (this *RaffleController) Save() {

	winner := this.buildWinner()
	prize := this.buildPrize()
	riddle := this.buildRiddle()
	raffle := this.buildRaffle(false, winner, prize, riddle)

	this.raffleService.Save(raffle)
	this.List()
}

func (this *RaffleController) Show() {
	id := bson.ObjectIdHex(this.Ctx.Input.Param(":id"))
	raffle := this.raffleService.FindRaffleById(id)

	this.Data["Raffle"] = &raffle
	this.Data["Statuses"] = []string{PENDING_STATUS, ACTIVE_STATUS, FINISHED_STATUS}
	this.Data["Hints"] = []int{1, 2, 3, 4, 5, 6}
	this.TplNames = "admin/raffle/show.tpl"
}

func (this *RaffleController) Update() {

	winner := this.buildWinner()
	prize := this.buildPrize()
	riddle := this.buildRiddle()
	raffle := this.buildRaffle(true, winner, prize, riddle)

	this.raffleService.Update(raffle)
	this.List()
}

func (this *RaffleController) buildRaffle(isUpdate bool, winner *models.Winner, prize *models.Prize, riddle *models.Riddle) (raffle *models.Raffle) {

	activeHints, err := this.GetInt("activeHints")
	if err != nil {
		panic(err)
	}

	id := bson.NewObjectId()
	if isUpdate {
		id = bson.ObjectIdHex(this.GetString("raffleId"))
	}

	raffle = &models.Raffle{
		Id:          id,
		Status:      this.GetString("status"),
		ActiveHints: activeHints,
		Winner:      winner,
		Prize:       prize,
		Riddle:      riddle,
	}

	return raffle
}

func (this *RaffleController) buildRiddle() (riddle *models.Riddle) {

	riddle = &models.Riddle{
		Answer:  this.GetString("answer"),
		Hints:   this.getHints(),
		Reasons: this.getReasons(),
	}

	return riddle
}

func (this *RaffleController) buildWinner() (winner *models.Winner) {

	winner = nil

	if this.GetString("email") != "" {
		winner = &models.Winner{
			Email: this.GetString("email"),
		}
	}

	return winner
}

func (this *RaffleController) buildPrize() (prize *models.Prize) {

	prize = nil

	if this.GetString("prizeURL") != "" {
		prize = &models.Prize{
			URL:   this.GetString("prizeURL"),
			Title: this.GetString("prizeTitle"),
			Photo: this.GetString("prizePhoto"),
		}
	}

	return prize
}

func (this *RaffleController) getHints() (hints []string) {

	hints = []string{
		this.GetString("hint1"),
		this.GetString("hint2"),
		this.GetString("hint3"),
		this.GetString("hint4"),
		this.GetString("hint5"),
		this.GetString("hint6"),
	}

	return hints
}

func (this *RaffleController) getReasons() (reasons []string) {

	reasons = []string{
		this.GetString("reason1"),
		this.GetString("reason2"),
		this.GetString("reason3"),
		this.GetString("reason4"),
		this.GetString("reason5"),
		this.GetString("reason6"),
	}

	return reasons
}
