package controllers

import (
	"github.com/mvpmvh/beego"
)

type baseController struct {
	beego.Controller
}

// Prepare is called prior to the baseController method
func (this *baseController) Prepare() {
}

// Finish is called once the baseController method completes
func (this *baseController) Finish() {

}

// ServeError prepares and serves an Error exception
func (this *baseController) ServeError(err error) {
	this.Data["json"] = struct {
		Error string `json:"Error"`
	}{err.Error()}

	this.Ctx.Output.SetStatus(500)
	this.ServeJson()
}
