package controllers

import (
	"bitbucket.org/rmmn/riddleraffle/services"
	"gopkg.in/mgo.v2/bson"
	"math/rand"
)

type MainController struct {
	baseController
	raffleService *services.RaffleService
	riddleService *services.RiddleService
}

func (this *MainController) Prepare() {

	if this.raffleService == nil {
		this.raffleService = new(services.RaffleService)
	}

	if this.riddleService == nil {
		this.riddleService = new(services.RiddleService)
	}
}

func (this *MainController) Welcome() {

	raffles := this.raffleService.FindActiveRafflesWithLimit(10)
	randomRaffle := raffles[rand.Intn(len(raffles))]
	this.Data["Raffle"] = randomRaffle
	this.TplNames = "index.tpl"
}

func (this *MainController) Guess() {

	id := bson.ObjectIdHex(this.GetString("raffleId"))
	answer := this.GetString("answer")
	raffle := this.raffleService.FindRaffleById(id)

	if raffle.Riddle.Answer == answer {
		//TODO: update db that there is 1 more correct answer
		this.TplNames = "success.tpl"
	} else {
		//TODO: update db with a new incorrect answer submitted
		this.Data["ErrorMessage"] = "Sorry, try again!"
		this.Data["Raffle"] = raffle
		this.TplNames = "index.tpl"
	}
}
