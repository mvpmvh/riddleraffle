package controllers

import (
	"bitbucket.org/rmmn/riddleraffle/models"
	"bitbucket.org/rmmn/riddleraffle/services"
	"github.com/mvpmvh/beego"
	"gopkg.in/mgo.v2/bson"
)

type RiddleController struct {
	beego.Controller
	riddleService *services.RiddleService
}

func (this *RiddleController) Prepare() {
	if this.riddleService == nil {
		this.riddleService = new(services.RiddleService)
	}
}

func (this *RiddleController) List() {
	riddles := this.riddleService.FindRiddlesWithLimit(10)
	this.Data["Riddles"] = &riddles
	this.TplNames = "admin/riddle/index.tpl"
}

func (this *RiddleController) Create() {
	this.TplNames = "admin/riddle/create.tpl"
}

func (this *RiddleController) Save() {

	riddle := &models.Riddle{
		Answer:  this.GetString("answer"),
		Hints:   this.getHints(),
		Reasons: this.getReasons(),
	}

	beego.Debug("Saving riddle: ", riddle)
	this.riddleService.Save(riddle)
	beego.Debug("Saved riddle")
	this.List()
}

func (this *RiddleController) Show() {
	id := bson.ObjectIdHex(this.Ctx.Input.Param(":id"))

	riddle := this.riddleService.FindRiddleById(id)
	beego.Debug("Found riddle ", riddle)
	this.Data["Riddle"] = &riddle
	this.TplNames = "admin/riddle/show.tpl"
}

func (this *RiddleController) getHints() (hints []string) {
	hints = []string{
		this.GetString("hint1"),
		this.GetString("hint2"),
		this.GetString("hint3"),
		this.GetString("hint4"),
		this.GetString("hint5"),
		this.GetString("hint6"),
	}

	return hints
}

func (this *RiddleController) getReasons() (reasons []string) {
	reasons = []string{
		this.GetString("reason1"),
		this.GetString("reason2"),
		this.GetString("reason3"),
		this.GetString("reason4"),
		this.GetString("reason5"),
		this.GetString("reason6"),
	}

	return reasons
}
