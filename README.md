# README #

### What Is this repo for? ###

* RiddleRaffle is for running a web application that offers hints to a riddle. Guessing the riddle results in an entry into a raffle prize.
* Version

### How do I get set up? ###

* [Install Golang](http://golang.org/doc/install)
* [Install Bee tool](http://beego.me/docs/install/bee.md)
* [Install MongoDB](http://docs.mongodb.org/manual/installation/)
* 'bee run' - to run application locally

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* mvhatch@gmail.com