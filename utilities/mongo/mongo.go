/*
	This package provides mongo connectivity support
*/

package mongo

import (
	"fmt"
	"github.com/mvpmvh/beego"
	"gopkg.in/mgo.v2"
	"time"
)

const (
	MASTER_SESSION    = "master"
	MONOTONIC_SESSION = "monotonic"
)

var (
	singleton *mongoManager // Reference to the singleton
)

type (
	// mongoConfiguration contains settings for initialization
	MongoConfiguration struct {
		Hosts    []string
		Timeout  time.Duration
		Database string
		Username string
		Password string
	}

	// mongoInstance contains dial and session information
	mongoInstance struct {
		mongoDBDialInfo *mgo.DialInfo
		mongoSession    *mgo.Session
	}

	// mongoManager manages a map of session
	mongoManager struct {
		sessions map[string]*mongoInstance
	}

	// MongoCall defines a type of function that can be used
	// to excecute code against MongoDB
	MongoCall func(*mgo.Collection) error
)

// Startup brings the manager to a running state
func Startup() (err error) {

	// If the system has already been started ignore the call
	if singleton != nil {
		return err
	}

	beego.Info("Startup...")

	mc := &MongoConfiguration{
		Hosts:    beego.AppConfig.Strings("hosts"),
		Database: beego.AppConfig.String("database"),
		Username: beego.AppConfig.String("dbUsername"),
		Password: beego.AppConfig.String("dbPassword"),
	}

	singleton = &mongoManager{
		sessions: map[string]*mongoInstance{},
	}

	if err = CreateSession("strong", MASTER_SESSION, mc); err == nil {
		if err = EnsureCollectionsIndexes(mc.Database, MASTER_SESSION, beego.AppConfig.Strings("collections")); err == nil {
			if err = CreateSession("monotonic", MONOTONIC_SESSION, mc); err == nil {
				if err = EnsureCollectionsIndexes(mc.Database, MONOTONIC_SESSION, beego.AppConfig.Strings("collections")); err == nil {
					beego.Info("Startup completed")
				}
			}
		}
	}

	return err
}

func EnsureCollectionsIndexes(databaseName string, sessionName string, collectionNames []string) (err error) {

	for _, collectionName := range collectionNames {
		indexKey := fmt.Sprintf("index_keys::%s", collectionName)
		if err = EnsureIndex(databaseName, sessionName, collectionName, beego.AppConfig.Strings(indexKey)); err != nil {
			break
		}
	}

	return err
}

// CreateSession creates a connection pool for use
func CreateSession(mode string, sessionName string, mc *MongoConfiguration) (err error) {

	beego.Debug("Creating session %+v", mc)

	if mc.Timeout == 0 {
		mc.Timeout = 60 * time.Second
	}

	mi := &mongoInstance{
		mongoDBDialInfo: &mgo.DialInfo{
			Addrs:    mc.Hosts,
			Timeout:  mc.Timeout,
			Database: mc.Database,
			Username: mc.Username,
			Password: mc.Password,
		},
	}

	// Establish the master session
	mi.mongoSession, err = mgo.DialWithInfo(mi.mongoDBDialInfo)
	if err != nil {
		return err
	}

	switch mode {
	case "strong":
		// Reads and writes will always be made to the master server using a
		// unique connection so that reads and writes are fully consistent,
		// ordered, and observing the most up-to-date data.
		// http://godoc.org/labix.org/v2/mgo#Session.SetMode
		mi.mongoSession.SetMode(mgo.Strong, true)
		break

	case "monotonic":
		// Reads may not be entirely up-to-date, but they will always see the
		// history of changes moving forward, the data read will be consistent
		// across sequential queries in the same session, and modifications made
		// within the session will be observed in following queries (read-your-writes).
		// http://godoc.org/labix.org/v2/mgo#Session.SetMode
		mi.mongoSession.SetMode(mgo.Monotonic, true)
	}

	// Have the session check for errors
	// http://godoc.org/labix.org/v2/mgo#Session.SetSafe
	mi.mongoSession.SetSafe(&mgo.Safe{})

	// Add the database to the map
	singleton.sessions[sessionName] = mi

	beego.Info("Session created")

	return err
}

// CopyMonotonicSession makes a copy of the monotonic session for client use
func CopyMonotonicSession() (*mgo.Session, error) {
	return CopySession(MONOTONIC_SESSION)
}

// CopySession makes a copy of the specified session for client use
func CopySession(sessionName string) (mongoSession *mgo.Session, err error) {

	beego.Debug("UseSession[%s]", sessionName)

	mongoSession, err = FindSession(sessionName)
	if err == nil {
		mongoSession = mongoSession.Copy()
		beego.Debug("Session[%s] copied", sessionName)
	}

	return mongoSession, err
}

// CloseSession puts the connection back into the pool
func CloseSession(mongoSession *mgo.Session) {

	beego.Debug("Closing session...", mongoSession)
	mongoSession.Close()
	beego.Debug("Session closed")
}

func FindSession(sessionName string) (mongoSession *mgo.Session, err error) {
	mongoInstance := singleton.sessions[sessionName]

	if mongoInstance == nil {
		err = fmt.Errorf("Unable to locate session %s", sessionName)
		beego.Error(err)
		return mongoSession, err
	}

	mongoSession = mongoInstance.mongoSession

	return mongoSession, err
}

func EnsureIndex(databaseName string, sessionName string, collectionName string, keys []string) (err error) {

	mongoSession, err := FindSession(sessionName)
	if err != nil {
		return err
	}

	db := mongoSession.DB(databaseName)
	collection := db.C(collectionName)

	index := mgo.Index{
		Key:        keys,
		Unique:     true,
		DropDups:   true,
		Background: true,
		Sparse:     true,
	}

	if err := collection.EnsureIndex(index); err != nil {
		beego.Error("unable to ensure index. %v", err)
	}

	return err
}
