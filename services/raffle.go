package services

import (
	"bitbucket.org/rmmn/riddleraffle/models"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type RaffleService struct {
	baseService
}

func (this *RaffleService) CollectionName() string {
	return "raffles"
}

func (this *RaffleService) FindRaffleById(id bson.ObjectId) (raffle *models.Raffle) {
	err := this.Execute(this.CollectionName(), func(c *mgo.Collection) error {
		return c.FindId(id).One(&raffle)
	})

	if err != nil {
		panic(err)
	}

	return raffle
}

func (this *RaffleService) FindActiveRafflesWithLimit(limit int) (raffles []*models.Raffle) {
	err := this.Execute(this.CollectionName(), func(c *mgo.Collection) error {
		return c.Find(bson.M{"status": "Active"}).Limit(limit).Iter().All(&raffles)
	})

	if err != nil {
		panic(err)
	}

	return raffles
}

func (this *RaffleService) FindRafflesWithLimit(limit int) (raffles []*models.Raffle) {
	err := this.Execute(this.CollectionName(), func(c *mgo.Collection) error {
		return c.Find(nil).Limit(limit).Iter().All(&raffles)
	})

	if err != nil {
		panic(err)
	}

	return raffles
}

func (this *RaffleService) Save(raffle *models.Raffle) {
	err := this.Execute(this.CollectionName(), func(c *mgo.Collection) error {
		return c.Insert(&raffle)
	})

	if err != nil {
		panic(err)
	}
}

func (this *RaffleService) Update(raffle *models.Raffle) {
	err := this.Execute(this.CollectionName(), func(c *mgo.Collection) error {
		return c.Update(bson.M{"_id": raffle.Id}, &raffle)
	})

	if err != nil {
		panic(err)
	}
}
