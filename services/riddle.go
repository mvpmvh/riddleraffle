package services

import (
	"bitbucket.org/rmmn/riddleraffle/models"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type RiddleService struct {
	baseService
}

func (this *RiddleService) CollectionName() string {
	return "riddles"
}

func (this *RiddleService) FindRiddleById(id bson.ObjectId) (riddle *models.Riddle) {
	err := this.Execute(this.CollectionName(), func(c *mgo.Collection) error {
		return c.FindId(id).One(&riddle)
	})

	if err != nil {
		panic(err)
	}

	return riddle
}

func (this *RiddleService) FindRiddlesWithLimit(limit int) (riddles []*models.Riddle) {
	err := this.Execute(this.CollectionName(), func(c *mgo.Collection) error {
		return c.Find(nil).Limit(limit).Iter().All(&riddles)
	})

	if err != nil {
		panic(err)
	}

	return riddles
}

func (this *RiddleService) Save(riddle *models.Riddle) {
	err := this.Execute(this.CollectionName(), func(c *mgo.Collection) error {
		return c.Insert(&riddle)
	})

	if err != nil {
		panic(err)
	}
}
