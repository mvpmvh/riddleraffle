package services

import (
	"bitbucket.org/rmmn/riddleraffle/utilities/mongo"
	_ "github.com/mvpmvh/beego"
	"gopkg.in/mgo.v2"
)

type baseService struct {
}

func (this *baseService) Prepare() (session *mgo.Session, err error) {
	session, err = mongo.CopyMonotonicSession()

	return session, err
}

func (this *baseService) Finish(session *mgo.Session) {
	mongo.CloseSession(session)
}

func (this *baseService) Collection(session *mgo.Session, collectionName string) *mgo.Collection {
	return session.DB("").C(collectionName)
}

func (this *baseService) Execute(collectionName string, mongoCall mongo.MongoCall) (err error) {

	session, err := this.Prepare()
	if err != nil {
		return err
	}

	defer this.Finish(session)

	collection := this.Collection(session, collectionName)
	err = mongoCall(collection)

	return err
}
