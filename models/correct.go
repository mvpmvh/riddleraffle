package models

import (
	"gopkg.in/mgo.v2/bson"
)

type Correct struct {
	Id, RaffleId bson.ObjectId `json:"_id" bson:"_id,omitempty"`
	Email        string
	Points       int
}
