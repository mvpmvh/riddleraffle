package models

type Riddle struct {
	Answer  string   `json:"answer" bson:"answer"`
	Hints   []string `json:"hints" bson:"hints"`
	Reasons []string `json:"reasons" bson:"reasons"`
}
