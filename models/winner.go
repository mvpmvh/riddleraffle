package models

import (
	"gopkg.in/mgo.v2/bson"
)

type Winner struct {
	Id    bson.ObjectId `json:"_id" bson:"_id,omitempty"`
	Email string        `json:"email" bson:"email"`
}
