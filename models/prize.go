// a package to organize all the different types
package models

import (
	"gopkg.in/mgo.v2/bson"
)

type Prize struct {
	Id    bson.ObjectId `json:"_id" bson:"_id,omitempty"`
	URL   string        `json:"prizeurl" bson:"prizeurl"`
	Title string        `json:"prizetitle" bson:"prizetitle"`
	Photo string        `json:"prizephoto" bson:"prizephoto"`
}
