package models

import (
	"gopkg.in/mgo.v2/bson"
)

type Raffle struct {
	Id          bson.ObjectId `json:"_id" bson:"_id"`
	Status      string        `json:"status" bson:"status"`
	ActiveHints int           `json:"activeHints" bson:"activeHints"`
	Winner      *Winner       `json:"winner" bson:"winner"`
	Prize       *Prize        `json:"prize" bson:"prize"`
	Riddle      *Riddle       `json:"riddle" bson:"riddle"`
}

func (raffle *Raffle) setStatus(status string) {
	raffle.Status = status
}

func (raffle *Raffle) setActiveHints(numHints int) {
	raffle.ActiveHints = numHints
}
